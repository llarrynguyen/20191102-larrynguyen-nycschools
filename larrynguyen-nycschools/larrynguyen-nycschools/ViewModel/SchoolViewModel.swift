//
//  SchoolViewModel.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//


import Foundation

class SchoolViewModel {
    var schools : [School] = []
    
    // These properties is for loading and pagination
    // When don't wanna to load all schools all the time
    let limit = Constants.downloadLimit
    let cellCountBuffer = 2
    var offset = 0 // initial offset
    var isLoading = false
    private var apiClient = APIClient()
    
    init(){
    }
    
    deinit {
        schools = []
    }
    
    func fetchSchools(limit: Int, offset: Int, completion: @escaping ([School]) -> ()) {
        if isLoading == true { return }
        self.isLoading = true
        apiClient.fetchData(endpoint: SchoolEndPoint.schoolList(limit: limit, offSet: offset)) {[weak self](schools: [School]) in
           
            guard let strongSelf = self else {return}
            strongSelf.schools.append(contentsOf: schools)
            completion(strongSelf.schools)
        }
    }
    
    func numbersOfItems () -> Int {
        return schools.count
    }
}

