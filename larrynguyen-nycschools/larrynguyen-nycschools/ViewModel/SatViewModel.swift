//
//  SatViewModel.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import Foundation

class SatViewModel {
    var satScoresList : [SatScores] = []
    var dbn: String
    private var apiClient = APIClient()
    
    init(_ dbn: String){
        self.dbn = dbn
    }
    
    func fetchSatScores(completion: @escaping ([SatScores]) -> ()) {
        debugPrint(dbn)
        apiClient.fetchData(endpoint: SatEndpoint.satScores(dbn: dbn)) {[weak self] (satScoresList: [SatScores]) in
            guard let strongSelf = self else {return}
            strongSelf.satScoresList = satScoresList
            completion(strongSelf.satScoresList)
        }
    }
}
