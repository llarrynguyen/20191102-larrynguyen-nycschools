//
//  SatScoresView.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import UIKit

class SatScoresView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var mathScoreLabel : UILabel!
    @IBOutlet var writtingScoreLabel: UILabel!
    @IBOutlet var readingScoreLabel: UILabel!
    @IBOutlet var schoolName: UILabel!
    
    var closeButtonClosure: (() -> ())?
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView(){
        let name = String(describing: type(of: self))
        let nib = UINib(nibName: name, bundle: .main)
        nib.instantiate(withOwner: self, options: nil)
        contentView.pinch(self)
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    func updateView(satScoresModel: SatScores) {
        mathScoreLabel.text = satScoresModel.sat_math_avg_score
        readingScoreLabel.text = satScoresModel.sat_critical_reading_avg_score
        writtingScoreLabel.text = satScoresModel.sat_writing_avg_score
        schoolName.text = satScoresModel.school_name
        layoutIfNeeded()
    }
    
    @IBAction func closeTapped() {
        closeButtonClosure?()
    }
}
