//
//  SchoolCollectionCell.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import UIKit

class SchoolCollectionCell: UICollectionViewCell {

    static let identifier = "SchoolCollectionCell"
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var letterLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initializing()
    }
    
    private func initializing() {
        nameLabel.numberOfLines = 0
        layer.cornerRadius = 4
        layer.masksToBounds = true
        let randomColor = UIColor.random()
        topView.backgroundColor = randomColor
        layer.borderColor = randomColor.cgColor
        layer.borderWidth = 2
    }
    
    func updateCell(with school: School) {
        nameLabel.text = school.school_name
        letterLabel.text = Utilities.getFirstLetter(from: school.school_name)
    }
}
