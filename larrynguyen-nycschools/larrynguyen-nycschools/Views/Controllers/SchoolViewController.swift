//
//  SchoolViewController.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//
import UIKit

/*
 To make it more practical use, we should in future create `Search` bar to help the
 users search for school they are looking for
 but for the scope of this challenge, we will not implement it
 Also, we should create some sort of caching for all the school that we already 
 */

class SchoolViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5)
        layout.itemSize = CGSize(width: (Constants.screenWidth - 20 )/2, height: 150)
        return layout
        
    }()
    
    private var viewModel: SchoolViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Schools in NYC"
        navigationController?.navigationBar.prefersLargeTitles = true

        viewModel = SchoolViewModel()
        setupCollectionView()
        viewModel.fetchSchools(limit: viewModel.limit, offset: viewModel.offset) { (schools) in
            mainQueue({
                self.collectionView.reloadData()
                self.viewModel.isLoading = false
                self.viewModel.offset += self.viewModel.limit
            })
        }
    }
    
    func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: SchoolCollectionCell.identifier, bundle: nil), forCellWithReuseIdentifier: SchoolCollectionCell.identifier)
        
        self.collectionView.setCollectionViewLayout(layout, animated: true)
    }

}

extension SchoolViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numbersOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == viewModel.schools.count - viewModel.cellCountBuffer {
            viewModel.fetchSchools(limit: viewModel.limit, offset: viewModel.offset) { (schools) in
                mainQueue {
                    self.collectionView.reloadData()
                    self.viewModel.isLoading = false
                    self.viewModel.offset += self.viewModel.limit
                }
            }
        }
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SchoolCollectionCell.identifier, for: indexPath) as? SchoolCollectionCell {
            cell.updateCell(with: viewModel.schools[indexPath.row])
            return cell
        }
        
        return UICollectionViewCell()
    }
}


extension SchoolViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: Constants.mainStoryboardName, bundle: nil)
        if let satViewController = storyboard.instantiateViewController(identifier: SchoolSatScoresViewController.identifier) as? SchoolSatScoresViewController {
            guard let dbn = viewModel.schools[indexPath.row].dbn else {return}
            satViewController.viewModel = SatViewModel(dbn)
            self.present(satViewController, animated: true, completion: nil)
        }
    }
}
