//
//  SchoolSatScoresViewController.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import UIKit

class SchoolSatScoresViewController: UIViewController {
    @IBOutlet weak var scoresView: SatScoresView!
    var viewModel: SatViewModel? = nil
    static let identifier = "SchoolSatScoresViewController"

    override func viewDidLoad() {
        super.viewDidLoad()
        scoresView.closeButtonClosure = {[weak self] in
            UIView.animate(withDuration: 1.5) {
                self?.dismiss(animated: true, completion: nil)
            }
        }
        if let viewModel = viewModel {
            viewModel.fetchSatScores { (satScoresArray) in
                mainQueue {
                    // If there is SAT data, update view, else show alert
                    if !satScoresArray.isEmpty {
                        self.scoresView.updateView(satScoresModel: viewModel.satScoresList[0])
                    } else {
                        let alert = UIAlertController(title: "Data not available", message: "SAT scores are not available", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
}
