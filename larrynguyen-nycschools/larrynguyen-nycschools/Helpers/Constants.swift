//
//  Constants.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import UIKit

struct Constants {
    static let downloadLimit = 20
    static let mainStoryboardName = "Main"
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height

}
