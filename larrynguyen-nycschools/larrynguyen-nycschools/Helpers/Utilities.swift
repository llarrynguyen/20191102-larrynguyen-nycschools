//
//  Utilities.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import Foundation

struct Utilities {
    static func getFirstLetter( from str: String?) -> String {
        return String(str?.first ?? "a")
    }
}
