//
//  School.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import Foundation

struct School : Decodable {
    // Parameters could be more `swift` here
    // But for now just use exact name from JSON
    var dbn: String?
    var school_name: String?
    var overview_paragraph: String?
    var location: String?
    var website: String?
}
