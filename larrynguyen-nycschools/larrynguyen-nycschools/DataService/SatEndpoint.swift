//
//  SatEndpoint.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import Foundation

enum SatEndpoint {
    case satScores(dbn: String)
    static let apiKey = "FPgqNXLMSdRDI4SopqkCDbBRN"
}

extension SatEndpoint : EndpointProtocol {
    var baseUrl: String {
        return Domain.cityOfNewyorkAPI.rawValue
    }
    
    var path: String {
        return "/resource/f9bf-2cp4.json"
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .satScores:
            return .get
        }
    }
    
    var parameters: [URLQueryItem]? {
        switch self {
        case .satScores(let dbn):
            return [
                URLQueryItem(name: "$$app_token", value: SatEndpoint.apiKey),
                URLQueryItem(name: "dbn", value: dbn)
            ]
        }
    }
    
    var request: URLRequest? {
        var urlComponents = URLComponents(string: baseUrl)
        urlComponents?.path = path
        urlComponents?.queryItems = parameters
        if let url = urlComponents?.url {
            var request = URLRequest(url: url)
            request.httpMethod = httpMethod.rawValue
            request.allHTTPHeaderFields = headers
            return request
        }
        return nil
    }
    
    var headers: HTTPHeaders? {
        return [
            "Content-Type" : "application/json"
        ]
    }
}

