//
//  SchoolEndPoint.swift
//  larrynguyen-nycschools
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import Foundation

enum SchoolEndPoint {
    case schoolList(limit: Int, offSet: Int)
    static let apiKey = "FPgqNXLMSdRDI4SopqkCDbBRN"
}

extension SchoolEndPoint : EndpointProtocol {
    var baseUrl: String {
        return Domain.cityOfNewyorkAPI.rawValue
    }
    
    var path: String {
        return "/resource/s3k6-pzi2.json"
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .schoolList:
            return .get
        }
    }
    
    var parameters: [URLQueryItem]? {
        switch self {
        case .schoolList( let limit, let offset):
            return [
                URLQueryItem(name: "$$app_token", value: SchoolEndPoint.apiKey),
                URLQueryItem(name: "$limit", value: "\(limit)"),
                URLQueryItem(name: "$offset", value: "\(offset)")            ]
        }
    }
    
    var request: URLRequest? {
        var urlComponents = URLComponents(string: baseUrl)
        urlComponents?.path = path
        urlComponents?.queryItems = parameters
        if let url = urlComponents?.url {
            var request = URLRequest(url: url)
            request.httpMethod = httpMethod.rawValue
            request.allHTTPHeaderFields = headers
            return request
        }
        return nil
    }
    
    var headers: HTTPHeaders? {
        return [
            "Content-Type" : "application/json"
        ]
    }
}

