//
//  larrynguyen_nycschoolsTests.swift
//  larrynguyen-nycschoolsTests
//
//  Created by Larry Nguyen on 11/2/19.
//  Copyright © 2019 Larry Nguyen. All rights reserved.
//

import XCTest
@testable import larrynguyen_nycschools

class larrynguyen_nycschoolsTests: XCTestCase {
    
    var viewModel: SchoolViewModel?

    override func setUp() {
        viewModel = SchoolViewModel()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSchoolListEndpoint() {
        // tests the URL that is created by the the endpoint
        let  endpoint = SchoolEndPoint.schoolList(limit: 20, offSet: 0)
        let url = endpoint.request?.url?.absoluteString
        XCTAssert(url == "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$$app_token=FPgqNXLMSdRDI4SopqkCDbBRN&$limit=20&$offset=0", "incorrect url")
    }
    
    func testSatScoresEndpoint() {
        let dbn = "28Q680"
        let endpoint = SatEndpoint.satScores(dbn: dbn)
        let url = endpoint.request?.url?.absoluteString
        XCTAssert(url == "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$$app_token=FPgqNXLMSdRDI4SopqkCDbBRN&dbn=\(dbn)", "incorrect url for SAT scores for school with dbn \(dbn)")
    }
}
